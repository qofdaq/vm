#!/bin/bash
if [ -z $1 ]; then
    echo backup.sh container name
    exit 1
fi

NAME=$1
NOW=$PWD
cd $(podman mount $NAME)
tar -czvf $NOW/$NAME.tar.gz --ignore-failed-read --exclude="venv" --exclude="__pycache__" --exclude=".*" $(ls -d root home opt/lfh/backup 2>/dev/null)
podman unmount $NAME
