#!/bin/bash
set -e

IP=$(curl ident.me)
CPUS=1.0
MEMORY=1024m
SIZE=3G
PIDS_LIMIT=10000

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    -n|--name)
      NAME="$2"
      shift
      shift
      ;;
    -p|--port)
      PORT="$2"
      shift
      shift
      ;;
    -P|--password)
      PASSWORD="$2"
      shift
      shift
      ;;
    --cpus)
      CPUS="$2"
      shift
      shift
      ;;
    --memory)
      MEMORY="$2"
      shift
      shift
      ;;
    --size)
      SIZE="$2"
      shift
      shift
      ;;
    --pids-limit)
      PIDS_LIMIT="$2"
      shift
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1")
      shift
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}"


if [[ $1 != @(ubuntu|debian) ]]; then
    echo "Set os name as argument: ubuntu, debian"
    exit 1
fi

OS=$1

if [ -z $PASSWORD ]; then
    PASSWORD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c16)
fi

if [ -z $NAME ]; then
    echo "Enter NAME"
    read -r NAME
fi

if [ -z $PORT ]; then
    echo "Enter PORT"
    read -r PORT
fi


if lsof -Pi :$PORT -sTCP:LISTEN; then
    echo -e "$PORT is used"
    exit 1
fi


name=$(podman run -d \
                 --network=host \
                 --restart always \
                 --cpus=$CPUS \
                 --memory=$MEMORY \
                 --storage-opt size=$SIZE \
                 --pids-limit $PIDS_LIMIT \
                 --cap-add audit_write \
                 --name $NAME\_$1\_$PORT localhost/vm:$OS)

echo "root:$PASSWORD" | podman exec -i $name chpasswd
podman exec -i $name sh -c "echo Port $PORT >> /etc/ssh/sshd_config.d/00-lukaszfreehosting.conf && systemctl enable ssh && systemctl restart ssh"

echo "OS:       \`$OS\`"
echo "IP:       \`$IP\`"
echo "PORT:     \`$PORT\`"
echo "USER:     \`root\`"
echo "PASSWORD: \`$PASSWORD\`"
echo
echo "CPU:        $CPUS"
echo "RAM:        $MEMORY"
echo "ROM:        $SIZE"
echo "PID:        $PIDS_LIMIT"
