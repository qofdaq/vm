#!/bin/bash
set -e


if [ -z "$1" ]; then
    echo "find \$pid"
    exit 1
fi

pid=$1
pid=`pstree -sgla $pid | grep systemd | tail -1 | grep -o '[[:digit:]]*'`
podman ps -q | xargs podman inspect --format '{{.State.Pid}}, {{.Name}}' | grep "^$pid"
