#!/bin/env bash
function get_info {
for i in $(podman ps --format "{{.Names}}"); do
echo $(podman exec $i df -h | grep overlay) $i
done
}
column -t -s' ' <(cat <(get_info) | sort -k 3 -h)
