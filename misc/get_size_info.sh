#!/bin/env sh
podman ps -sa --format "{{.Names}}  \t  {{.Size}}" | sort -k 2 -h  > size_info
