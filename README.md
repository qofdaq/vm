# VM
Virtual Machine with sshd from docker image

## Installation
Install podman with any method.
Run build of images with `build.sh`

## Usage
Use `create.sh` to create VM instance <br />
Specify first argument as OS <br />
Available OSs:
 - ubuntu
 - debian
