#!/bin/bash
FULL_PATH_TO_SCRIPT="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$FULL_PATH_TO_SCRIPT")"

IFS=' ' read -r -a array <<< $(echo ubuntu debian)

for x in "${array[@]}"
do echo building $x
podman build $SCRIPT_DIRECTORY/$x\_min -t vm:$x\_min
podman build $SCRIPT_DIRECTORY/$x -t vm:$x
done
